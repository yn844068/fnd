package uk.ac.reading.fnd.kaiml;

import java.io.File;
import java.io.FileWriter;
import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import uk.ac.reading.fnd.kaiml.jdbc.JdbcManager;

public class BuildNetworkFromUserFollowers {
	private static ObjectMapper objectMapper = new ObjectMapper();
	public static void main(String[] args) throws Exception {
		//processFilesAtLocation("D:\\data-science\\project\\downloads-server\\july17\\followers");
		//processFilesAtLocation("D:\\data-science\\project\\experiments\\followers");
		generaateCSVforNetworkx("D:\\data-science\\project\\downloads-server\\combined-followers");
	}
	
	/**
	 * the scan through the followers folder, get all the users, parse it and save it to db
	 * 
	 * @param directory
	 * @throws Exception
	 */
	public static void processFilesAtLocation(String directory) throws Exception {
		List<String> files = Utils.listFilesInDirectory(directory);
		System.out.println("total number of files : "+files.size());
		for(String fileName : files) {
			System.out.println(fileName);
			String[] parts = fileName.split("_");
			System.out.println(parts[1]);		
			String id = parts[1];
			JsonNode jsonNode = objectMapper.readTree(new File(fileName));
			if (jsonNode != null) {
				JsonNode followers = jsonNode.get("users");
				
				if (followers != null) {
					System.out.println(fileName+"  number of followers for this user "+followers.size());
					for (JsonNode follower : followers) {
						String fid = follower.get("id").asText();
						String fName = follower.get("screen_name").asText();
						String createdAt = follower.get("created_at").asText();
						long fFollowerCount = follower.get("followers_count").asLong();
						long fFriendsCount = follower.get("friends_count").asLong();
						long fListedCount = follower.get("listed_count").asLong();
						JdbcManager.insertTwitterUserFollower(id, fid, fName, createdAt, fFollowerCount, fFriendsCount, fListedCount);
						
					}
				}

			}
			
			
		}
	

	}
	
	
	/**
	 * the generated CSV will be of the form follower, targetid
	 * 
	 * @param directory
	 * @throws Exception
	 */
	public static void generaateCSVforNetworkx(String directory) throws Exception {
		String twitterNetworkFile = "D:\\data-science\\project\\experiments\\combined_twitter_network1.csv";
		List<String> files = Utils.listFilesInDirectory(directory);
		System.out.println("total number of files : "+files.size());
		FileWriter writer = new FileWriter(twitterNetworkFile);
		for(String fileName : files) {
			System.out.println(fileName);
			String[] parts = fileName.split("_");
			System.out.println(parts[1]);
			
			JsonNode jsonNode = objectMapper.readTree(new File(fileName));
			if (jsonNode != null) {
				JsonNode followers = jsonNode.get("users");
				
				if (followers != null) {
					System.out.println(fileName+"  number of followers for this user "+followers.size());
					for (JsonNode follower : followers) {
						long id = follower.get("id").asLong();
						writer.write(id +", "+ parts[1]	+"\n");	
					}
				}

			}
			
			
		}
		if (writer != null) {
			writer.flush();
			writer.close();
		}

	}
}
