package uk.ac.reading.fnd.kaiml;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

public class SampleBasedOnColumn {

	public static void main2(String[] args) throws IOException {
		System.out.println("hello world");
		String smallFile = "D:\\data-science\\project\\experiments\\twitter-data-p1-1k.csv";
		String largeFile = "D:\\data-science\\project\\experiments\\twitter-data-p2-all.csv";
		String targetFile = "D:\\data-science\\project\\experiments\\twitter-data-p2-1k.csv";
		FileWriter writer = null;
		long count = 0;
		long startTime = System.currentTimeMillis(); 
		try (Reader reader = new FileReader(smallFile);
				CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT.withQuote('"').withDelimiter(','))) {
			writer = new FileWriter(targetFile);
			// Iterate over the records in the CSV file
			for (CSVRecord record : csvParser) {
				BufferedReader p2Reader = new BufferedReader(new FileReader(largeFile));
				String tweetId = "";
				// Process each field in the record
				for (String field : record) {
					// System.out.println(field);
					tweetId = field;
					break;
				}
				System.out.println(tweetId+" "+count);
				count++;
				String line = null;
				while ((line = p2Reader.readLine()) != null) {
					if (line.contains(tweetId)) {
						writer.write(line + "\n");
					}
				}

			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (writer != null) {
				writer.flush();
				writer.close();
			}
		}
		long endTime = System.currentTimeMillis();
		long executionTime = endTime - startTime;
		System.out.println("Execution time: " + executionTime + " milliseconds");
	}

	
	/**
	 * 
	 * Based on hashmap approach.
	 * @param args
	 * @throws IOException
	 */
	public static void main3(String[] args) throws IOException {
		System.out.println("hello world");
		String smallFile = "D:\\data-science\\project\\experiments\\twitter-data-p1-1k.csv";
		String largeFile = "D:\\data-science\\project\\experiments\\twitter-data-p2-all.csv";
		String targetFile = "D:\\data-science\\project\\experiments\\twitter-data-p2-1kc.csv";
		FileWriter writer = null;
		long count = 0;
		long startTime = System.currentTimeMillis(); 
		try (BufferedReader smallFileReader = new BufferedReader(new FileReader(smallFile))) {
			writer = new FileWriter(targetFile);
			BufferedReader p2Reader = new BufferedReader(new FileReader(largeFile));
			String line = null;
			Map<String, String> map = new HashMap<String, String>();
			while ((line = p2Reader.readLine()) != null) {
				String[] columns = line.split(",");
				//String tweet =  columns[0].substring(1, columns[0].length() - 1);
				map.put(columns[0], line);
			}
			System.out.println("============== read items into map "+map.size());
			while ((line = smallFileReader.readLine()) != null) {
				 String[] columns = line.split(",");
				 if(map.containsKey(columns[0])) {
					 writer.write(line + "\n");
				 }
			}
			

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (writer != null) {
				writer.flush();
				writer.close();
			}
		}
		long endTime = System.currentTimeMillis();
		long executionTime = endTime - startTime;
		System.out.println("Execution time: " + executionTime + " milliseconds");
	}
	
	/*
	 * Generic function. 
	 * Give the targetSize (number of lines required) 
	 * Two files are generated in both p1 and p2 format.
	 */
	public static void main(String[] args) throws IOException {
		System.out.println("hello world");
		String p1File = "D:\\data-science\\project\\experiments\\tweets-p1-all-notext.csv";
		String p2File = "D:\\data-science\\project\\experiments\\tweets-p2-all.csv";
		FileWriter writer1 = null;
		FileWriter writer2 = null;
		long targetSize = 1400000;
		String targetFileP1 = "D:\\data-science\\project\\experiments\\tweets-exp-p1-"+targetSize+".csv";
		String targetFileP2 = "D:\\data-science\\project\\experiments\\tweets-exp-p2-"+targetSize+".csv";
		long count = 0;
		long startTime = System.currentTimeMillis(); 
		try  {
			BufferedReader p1Reader = new BufferedReader(new FileReader(p1File));
			writer1 = new FileWriter(targetFileP1);
			writer2 = new FileWriter(targetFileP2);
			BufferedReader p2Reader = new BufferedReader(new FileReader(p2File));
			String line = null;
			Map<String, String> map = new HashMap<String, String>();
			while ((line = p2Reader.readLine()) != null) {
				if(line.contains("flag_real")) {
					 writer2.write(line + "\n");
				}
				String[] columns = line.split(",");
				//String tweet =  columns[0].substring(1, columns[0].length() - 1);
				map.put(columns[0], line);
			}
			System.out.println("============== read items into map "+map.size());
			while ((line = p1Reader.readLine()) != null) {
				//this line is for adding header
				if(line.contains("user_created_date") && line.contains("author_id") ) {
					 writer1.write(line + "\n");
				}
				count++;
				 String[] columns = line.split(",");
				 //System.out.println(columns[0]+"== "+columns.length);
				 if(columns.length == 6 && (line.contains(",False") || line.contains(",True") )) {
					 if(map.containsKey(columns[0])) {
						 writer1.write(line + "\n");
						 writer2.write(map.get(columns[0]) + "\n");
					 }					 
				 }
				 if(targetSize < count) {
					 break;
				 }
			}
			

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (writer1 != null) {
				writer1.flush();
				writer1.close();
			}
			if (writer2 != null) {
				writer2.flush();
				writer2.close();
			}
		}
		long endTime = System.currentTimeMillis();
		long executionTime = endTime - startTime;
		System.out.println("Execution time: " + executionTime + " milliseconds");
	}

}
