package uk.ac.reading.fnd.kaiml.rest;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import uk.ac.reading.fnd.kaiml.ConfigurationProperties;

public class ApiClient {
	private OkHttpClient client;
	public ApiClient() {
		client = new OkHttpClient().newBuilder().build();
		
	}
	
	public Response callApi(String url) throws Exception {
		Request request = new Request.Builder()
				  .url(url)
				  .method("GET", null)
				  .addHeader("Authorization", "Bearer "+ConfigurationProperties.properties.getProperty("btoken"))
				  .build();
		//System.out.println("hitting api with "+ConfigurationProperties.properties.getProperty("btoken"));
		Response response = client.newCall(request).execute();
		return response;
	}
}
