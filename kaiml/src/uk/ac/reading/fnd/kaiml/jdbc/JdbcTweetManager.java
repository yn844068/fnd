package uk.ac.reading.fnd.kaiml.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class JdbcTweetManager {

	private static Connection conn = null;
	public static void main(String[] args) throws Exception {
		
		System.out.println("hi JdbcManager " );
		List<String> userIds = getUsersFromQuery();
		System.out.println(userIds.size());
	}
    public static Connection getConnection() throws SQLException {
    	if(conn == null) {
    		System.out.println("creating connection..");
    		//conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/twit3", "cloner", "team123");
    		conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/livedb", "cloner", "team123");
    	}
        return conn;
    }
	
    public static List<String> getUsersFromQuery() throws Exception {
    	List<String> users = new ArrayList<String>();
    	if(conn == null) {
    		conn = getConnection();
    	}
    	String sql = "select distinct author_id\r\n" + 
    			"from tweets\r\n" + 
    			"where id in (\r\n" + 
    			"	SELECT  tweet_id \r\n" + 
    			"	FROM article_tweet \r\n" + 
    			"	WHERE article_id IN (	\r\n" + 
    			"		SELECT id\r\n" + 
    			"		from (\r\n" + 
    			"		SELECT articles.id, articles.title, COUNT(article_tweet.tweet_id) AS tweet_count FROM articles \r\n" + 
    			"		LEFT JOIN article_tweet ON articles.id = article_tweet.article_id \r\n" + 
    			"		WHERE articles.flag_real = true\r\n" + 
    			"		GROUP BY articles.id, articles.title \r\n" + 
    			"		HAVING COUNT(article_tweet.tweet_id) > 0 AND COUNT(article_tweet.tweet_id) < 500\r\n" + 
    			"		ORDER BY tweet_count DESC \r\n" + 
    			"		LIMIT 1000\r\n" + 
    			")subquery )	)";
    	
    	PreparedStatement stmt = conn.prepareStatement(sql);
        ResultSet rs = stmt.executeQuery();

       while (rs.next()) {
           String id = rs.getString("author_id");
           users.add(id);
       }
       
    	return users;
    }

    
    public static List<String> getAllUsers() throws Exception {
    	List<String> users = new ArrayList<String>();
    	if(conn == null) {
    		conn = getConnection();
    	}
    	String sql = "select id from users";
    	
    	PreparedStatement stmt = conn.prepareStatement(sql);
        ResultSet rs = stmt.executeQuery();

       while (rs.next()) {
           String id = rs.getString("id");
           users.add(id);
       }
       
    	return users;
    }
    
    public static List<String> getAllTweets() throws Exception {
    	List<String> tweets = new ArrayList<String>();
    	if(conn == null) {
    		conn = getConnection();
    	}
    	String sql = "select id from tweets order by id";
    	
    	PreparedStatement stmt = conn.prepareStatement(sql);
        ResultSet rs = stmt.executeQuery();

       while (rs.next()) {
           String id = rs.getString("id");
           tweets.add(id);
       }
       
    	return tweets;
    }
    
    public static List<String> getUsersFromQuery(String query) throws Exception {
    	List<String> users = new ArrayList<String>();
    	if(conn == null) {
    		conn = getConnection();
    	}
    	PreparedStatement stmt = conn.prepareStatement(query);
        ResultSet rs = stmt.executeQuery();

       while (rs.next()) {
           String id = rs.getString("id");
           users.add(id);
       }
       
    	return users;
    }
    
    
    
}
