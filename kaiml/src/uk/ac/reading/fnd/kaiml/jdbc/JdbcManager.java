package uk.ac.reading.fnd.kaiml.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import uk.ac.reading.fnd.kaiml.ConfigurationProperties;
import uk.ac.reading.fnd.kaiml.NewsItem;
import uk.ac.reading.fnd.kaiml.models.InvalidResource;
import uk.ac.reading.fnd.kaiml.models.Tweet;
import uk.ac.reading.fnd.kaiml.models.User;

public class JdbcManager {
	private static Connection conn = null;
	public static void main(String[] args) throws Exception {
		
		System.out.println("hi JdbcManager " );
		
	}
    public static Connection getConnection() throws SQLException {
    	if(conn == null) {
    		System.out.println("creating connection..");
    		//conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/twit3", "cloner", "team123");
    		conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/livedb", "cloner", "team123");
    	}
        return conn;
    }
    
    public static List<NewsItem> getArticles(String sql) throws Exception {
    	List<NewsItem> articles = new ArrayList<NewsItem>();
    	if(conn == null) {
    		conn = getConnection();
    	}
    	
    	PreparedStatement stmt = conn.prepareStatement(sql);
        ResultSet rs = stmt.executeQuery();

       while (rs.next()) {
           String id = rs.getString("id");
           String url = rs.getString("url");
           String title = rs.getString("title");
           Boolean flagReal = rs.getBoolean("flag_real");
           NewsItem article = new NewsItem(id, url, title, flagReal, null);
           articles.add(article);
       }
       
    	return articles;
    }
    
    public static List<User> getUsers(String sql) throws Exception {
    	List<User> users = new ArrayList<User>();
    	if(conn == null) {
    		conn = getConnection();
    	}
    	
    	PreparedStatement stmt = conn.prepareStatement(sql);
        ResultSet rs = stmt.executeQuery();

       while (rs.next()) {
           String id = rs.getString("id");
           String username = rs.getString("username");
           String name = rs.getString("name");
           User user = new User(id, username, name);
           users.add(user);
       }
       
    	return users;
    }


    public static List<String> getTweetIdsByArticleId(String articleId) throws Exception {
    	List<String> tweetIds = new ArrayList<String>();
    	if(conn == null) {
    		conn = getConnection();
    	}
    	
    	PreparedStatement stmt = conn.prepareStatement("select tweet_id from article_tweet where article_id=?");
    	stmt.setString(1, articleId); 
        ResultSet rs = stmt.executeQuery();

       while (rs.next()) {
           String tweetId = rs.getString("tweet_id");
           tweetIds.add(tweetId);
       }
       
    	return tweetIds;
    }
    public static void insertArticle(NewsItem article) throws SQLException {
    	if(conn == null) {
    		conn = getConnection();
    	}
    	PreparedStatement  pstmt = null;
    	try {
			pstmt = conn.prepareStatement("INSERT INTO articles (id, url, title, flag_real) VALUES (?, ?, ?, ?)");
			pstmt.setString(1, article.getId());
			
            pstmt.setString(2, article.getNewsUrl());
            pstmt.setString(3, article.getTitle());
            pstmt.setBoolean(4, article.getFlagReal());
            int rowsInserted = pstmt.executeUpdate();
            //System.out.println(rowsInserted + " row(s) inserted.");
            
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.getErrorCode());
		} finally {
			if (pstmt != null) {
                try {
					pstmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
            }
		}

    }
    
    public static void insertArticleTweet(String articleId, Set<String> tweetIds) throws SQLException {
    	if(conn == null) {
    		conn = getConnection();
    	}
    	PreparedStatement  pstmt = null;
    	try {
			pstmt = conn.prepareStatement("INSERT INTO article_tweet (article_id, tweet_id) VALUES (?, ?)");
			
			for (String tweetId: tweetIds) {
			    pstmt.setString(1, articleId);  
			    pstmt.setString(2, tweetId);
			    pstmt.executeUpdate(); // Execute the statement for the current iteration
			}
			
			  
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pstmt != null) {
                try {
					pstmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
            }
		}

    }
    
    
    public static void insertTwitterTweets(Tweet tweet) throws SQLException {
    	if(conn == null) {
    		conn = getConnection();
    	}
    	PreparedStatement  pstmt = null;
    	try {
			pstmt = conn.prepareStatement("INSERT INTO tweets (id, text, conversation_id, author_id, possibly_sensitive, edit_history_tweet_ids) VALUES (?, ?, ?, ?, ?, ?)");
			pstmt.setString(1, tweet.getId());
            pstmt.setString(2, tweet.getText());
            pstmt.setString(3, tweet.getConversationId());
            pstmt.setString(4, tweet.getAuthorId());
            pstmt.setBoolean(5, tweet.getPossiblySensitive());
            pstmt.setString(6, tweet.getEditHistoryTweetIds());
            int rowsInserted = pstmt.executeUpdate();
            //System.out.println(rowsInserted + " row(s) inserted.");
            
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("ERROR inserting "+tweet);
			
		} finally {
			if (pstmt != null) {
                try {
					pstmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
            }
		}

    }
    
    
    public static void insertTwitterUser(User user) throws SQLException {
    	if(conn == null) {
    		conn = getConnection();
    	}
    	PreparedStatement  pstmt = null;
    	try {
			pstmt = conn.prepareStatement("INSERT INTO users (id, username, name, verified, created_at, verified_type) VALUES (?, ?, ?, ?, ?, ?)");
			pstmt.setString(1, user.getId());
            pstmt.setString(2, user.getUsername());
            pstmt.setString(3, user.getName());
            pstmt.setBoolean(4, user.getVerified());
            pstmt.setString(5, user.getCreatedAt());
            pstmt.setString(6, user.getVerifiedType());
            int rowsInserted = pstmt.executeUpdate();
            //System.out.println(rowsInserted + " row(s) inserted.");
            
		} catch (SQLException e) {
			//e.printStackTrace();
			System.out.println("ERROR inserting "+user);
		} finally {
			if (pstmt != null) {
                try {
					pstmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
            }
		}

    }
    
    
    
    public static void insertTwitterErrors(InvalidResource error) throws SQLException {
    	if(conn == null) {
    		conn = getConnection();
    	}
    	PreparedStatement  pstmt = null;
    	try {
			pstmt = conn.prepareStatement("INSERT INTO invalid_resources(resource_id, parameter, resource_type, section, title, value, detail, type) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
			pstmt.setString(1, error.getResourceId());
            pstmt.setString(2, error.getParameter());
            pstmt.setString(3, error.getResourceType());
            pstmt.setString(4, error.getSection());
            pstmt.setString(5, error.getTitle());
            pstmt.setString(6, error.getValue());
            pstmt.setString(7, error.getDetail());
            pstmt.setString(8, error.getType());
            int rowsInserted = pstmt.executeUpdate();
            //System.out.println(rowsInserted + " row(s) inserted.");
            
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.getErrorCode());
		} finally {
			if (pstmt != null) {
                try {
					pstmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
            }
		}

    }
    
    
    
    public static void updateTwitterUser(long followerCount, long followingCount, long tCount, long listCount, String id) throws SQLException {
    	if(conn == null) {
    		conn = getConnection();
    	}
    	PreparedStatement  pstmt = null;
    	try {
			pstmt = conn.prepareStatement("UPDATE users SET followers_count = ?, following_count = ?, tweet_count = ?, listed_count = ? WHERE id = ?");
			pstmt.setLong(1, followerCount);
            pstmt.setLong(2, followingCount);
            pstmt.setLong(3, tCount);
            pstmt.setLong(4, listCount);
            pstmt.setString(5, id);
            int rowsInserted = pstmt.executeUpdate();
            //System.out.println(rowsInserted + " row(s) inserted.");
            
		} catch (SQLException e) {
			//e.printStackTrace();
			System.out.println("ERROR updating   "+id);
		} finally {
			if (pstmt != null) {
                try {
					pstmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
            }
		}

    }

    
    public static void updateTweetsWithStatistics(long retweetCount, long replyCount, long likeCount, long quoteCount, long impressionCount, String id) throws SQLException {
    	if(conn == null) {
    		conn = getConnection();
    	}
    	PreparedStatement  pstmt = null;
    	try {
			pstmt = conn.prepareStatement("UPDATE tweets SET retweet_count = ?, reply_count = ?, like_count = ?, quote_count = ?, impression_count=? WHERE id = ?");
			pstmt.setLong(1, retweetCount);
            pstmt.setLong(2, replyCount);
            pstmt.setLong(3, likeCount);
            pstmt.setLong(4, quoteCount);
            pstmt.setLong(5, impressionCount);
            pstmt.setString(6, id);
            int rowsInserted = pstmt.executeUpdate();
            //System.out.println(rowsInserted + " row(s) inserted.");
            
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("ERROR updating   "+id);
		} finally {
			if (pstmt != null) {
                try {
					pstmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
            }
		}

    }

    
    public static void updateTweetsOthers(String createdAt,  String lang, String id) throws SQLException {
    	if(conn == null) {
    		conn = getConnection();
    	}
    	PreparedStatement  pstmt = null;
    	try {
			pstmt = conn.prepareStatement("UPDATE tweets SET created_at = ?,  tweetlanguage = ? WHERE id = ?");
			pstmt.setString(1, createdAt);
            pstmt.setString(2, lang);
            pstmt.setString(3, id);
            int rowsInserted = pstmt.executeUpdate();
            //System.out.println(rowsInserted + " row(s) inserted.");
            
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("ERROR updating   "+id);
		} finally {
			if (pstmt != null) {
                try {
					pstmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
            }
		}

    }
    
    public static void insertTwitterUserFollower(String id, String fid, String fName, String createdAt,
    		long fFollowerCount, long fFriendsCount, long fListedCount) throws SQLException {
    	if(conn == null) {
    		conn = getConnection();
    	}
    	PreparedStatement  pstmt = null;
    	try {
			pstmt = conn.prepareStatement("INSERT INTO followers (id, fid, fname, f_followercount, f_friendscount, f_listedcount, created_at) VALUES (?, ?, ?, ?, ?, ?, ?)");
			pstmt.setString(1, id);
			pstmt.setString(2, fid);
            pstmt.setString(3, fName);
            pstmt.setLong(4, fFollowerCount);
            pstmt.setLong(5, fFriendsCount);
            pstmt.setLong(6, fListedCount);
            pstmt.setString(7, createdAt);
            int rowsInserted = pstmt.executeUpdate();
            //System.out.println(rowsInserted + " row(s) inserted.");
            
		} catch (SQLException e) {
			//e.printStackTrace();
			System.out.println("ERROR inserting "+id+"  "+fName);
			e.printStackTrace();
		} finally {
			if (pstmt != null) {
                try {
					pstmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
            }
		}

    }
    

}
