package uk.ac.reading.fnd.kaiml;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import uk.ac.reading.fnd.kaiml.jdbc.JdbcManager;

public class Phase2 {
	public static ObjectMapper objectMapper = new ObjectMapper();

	public static void main(String[] args) throws Exception {
		System.out.println("Iterating over all tweets and get secondary information");
		long startTime = System.currentTimeMillis(); 
		processTweetBatchFilesAtLocationForCreatedDate("D:\\data-science\\project\\experiments\\tweets3");
		long endTime = System.currentTimeMillis();
		long executionTime = endTime - startTime;
		System.out.println("Execution time: " + executionTime + " milliseconds");
		//processTweetBatchFilesAtLocation("D:\\data-science\\project\\experiments\\test-update-tweet-stats");
	}

	
	public static void processTweetBatchFilesAtLocation(String directory) throws Exception {
		List<String> files = Utils.listFilesInDirectory(directory);
		System.out.println("total number of files : "+files.size());
		for(String fileName : files) {
			JsonNode jsonNode = objectMapper.readTree(new File(fileName));
			
			if (jsonNode != null) {
				JsonNode data = jsonNode.get("data");
				
				if (data != null) {
					System.out.println(fileName+"  number of tweets in batch "+data.size());
					for (JsonNode tweet : data) {
						JsonNode metrics = tweet.get("public_metrics");
						String id = tweet.get("id").asText();
						String text = tweet.get("text").asText();
						if (metrics != null) {
							long rtc = metrics.get("retweet_count").asLong();
							long rpc = metrics.get("reply_count").asLong();
							long lc = metrics.get("like_count").asLong();
							long qc = metrics.get("quote_count").asLong();
							long ic = metrics.get("impression_count").asLong();
							//System.out.println(fileName+": "+id+","+rtc+"," +rpc+ "," + lc+ ","+ qc+","+ic);
							//UPDATE tweets SET retweet_count = ?, reply_count = ?, like_count = ?, quote_count = ?, impression_count=? WHERE id = ?
							JdbcManager.updateTweetsWithStatistics(rtc, rpc, lc, qc, ic, id);
						}

					}
				}

			}
		}
	}
	
	public static void processTweetBatchFilesAtLocationForCreatedDate(String directory) throws Exception {
		List<String> files = Utils.listFilesInDirectory(directory);
		System.out.println("total number of files : "+files.size());
		for(String fileName : files) {
			JsonNode root = objectMapper.readTree(new File(fileName));
			
			if (root != null) {
				
				for (JsonNode tweet : root) {
					String id = tweet.get("id").asText();
					String createdAt = tweet.get("created_at").asText();
					String lang = tweet.get("lang").asText();
					JdbcManager.updateTweetsOthers(createdAt, lang, id);
				}
			}
			System.out.println(root.size());
		}
	}
}
