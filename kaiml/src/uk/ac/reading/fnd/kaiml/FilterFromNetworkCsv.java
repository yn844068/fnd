package uk.ac.reading.fnd.kaiml;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Filter only the network related nodes from the phase 2 dataset
 * @author Maymon
 *
 */
public class FilterFromNetworkCsv {

	public static void main(String[] args) throws IOException {
		System.out.println("hello world");
		String p1File = "D:\\data-science\\project\\experiments\\graph\\graph-wo-leaf-2l.csv";
		//String p1File = "D:\\data-science\\project\\experiments\\graph\\temp.csv";
		String p2File = "D:\\data-science\\project\\experiments\\tweets-p2-all.csv";
		//String p2File = "D:\\data-science\\project\\experiments\\tweets-exp-p2-100.csv";
		
		FileWriter writer2 = null;
		String targetFileP2 = "D:\\data-science\\project\\experiments\\graph\\tweets-exp-p2-nw-filtered-small.csv";
		long count = 0;
		long startTime = System.currentTimeMillis(); 
		try  {
			BufferedReader p1Reader = new BufferedReader(new FileReader(p1File));
			writer2 = new FileWriter(targetFileP2);
			BufferedReader p2Reader = new BufferedReader(new FileReader(p2File));
			String line = null;
			Map<String, List<String>> authorMap = new HashMap<String, List<String>>();
			while ((line = p2Reader.readLine()) != null) {
				String[] columns = line.split(",");
				String authorId = columns[7].replace("\"","");
				//System.out.println(authorId);
				if(authorMap.containsKey(authorId)) {
					//System.out.println(authorId);
					authorMap.get(authorId).add(line);
				}else {
					List<String> list = new ArrayList<String>();
					list.add(line);
					authorMap.put(authorId, list);
				}
			}
			System.out.println("total of "+authorMap.size());
			while ((line = p1Reader.readLine()) != null) {
				count++;
				String[] columns = line.split(",");
				String targetId = columns[1];
				List<String> rows = authorMap.get(targetId);
				if(rows != null) {
					for(String row : rows) {
						writer2.write(row + "\n");
					}
				}
			}


		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (writer2 != null) {
				writer2.flush();
				writer2.close();
			}
		}
		long endTime = System.currentTimeMillis();
		long executionTime = endTime - startTime;
		System.out.println("Execution time: " + executionTime + " milliseconds");
	}

}
