package uk.ac.reading.fnd.kaiml.models;

public class Tweet {
	private String id;
	private String text;
	private String conversationId;
	private String authorId;
	private Boolean possiblySensitive;
	private String editHistoryTweetIds;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getConversationId() {
		return conversationId;
	}
	public void setConversationId(String conversationId) {
		this.conversationId = conversationId;
	}
	public String getAuthorId() {
		return authorId;
	}
	public void setAuthorId(String authorId) {
		this.authorId = authorId;
	}
	public Boolean getPossiblySensitive() {
		return possiblySensitive;
	}
	public void setPossiblySensitive(Boolean possiblySensitive) {
		this.possiblySensitive = possiblySensitive;
	}
	public String getEditHistoryTweetIds() {
		return editHistoryTweetIds;
	}
	public void setEditHistoryTweetIds(String editHistoryTweetIds) {
		this.editHistoryTweetIds = editHistoryTweetIds;
	}
	
	@Override
	public String toString() {
		return id + " "+ text+" "+conversationId+" "+authorId;
	}
}
