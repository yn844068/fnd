package uk.ac.reading.fnd.kaiml;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import okhttp3.Response;
import uk.ac.reading.fnd.kaiml.jdbc.JdbcManager;
import uk.ac.reading.fnd.kaiml.jdbc.JdbcTweetManager;
import uk.ac.reading.fnd.kaiml.rest.ApiClient;

class FollowerCheck {
	public static ObjectMapper objectMapper = new ObjectMapper();
	public static String fileLocation = "D:\\data-science\\project\\downloads-server\\may25\\followers";

	public static String fileLocationUsers = "D:\\data-science\\project\\downloads-server\\may29\\users";

	public static void main(String[] args) throws Exception {
		String fileNameUsers = "resources//users-with-followers-75-100.csv";
		if(args.length == 1) {
			fileNameUsers = "resources//"+args[0];
		}
		System.out.println("===boom "+fileNameUsers);
		
		downloadFollowersAgain(fileNameUsers);
		
	}
	public static void downloadFollowersAgain(String fileNameUsers) throws Exception {
		
		List<String> userIds = Utils.readFileIntoList(fileNameUsers);
		System.out.println(userIds.size());
		int count = 0;
		List<String> files = getFileNamesInDirectory("followers");
		List<String> alreadyDownloadedFiles = new ArrayList<String>();
		for(String downloadedFile : files) {
			String parts[] = downloadedFile.split("_");
			alreadyDownloadedFiles.add(parts[1]);
		}
		System.out.println(alreadyDownloadedFiles);
		

		for(String userId : userIds) {
			count++;
			if(alreadyDownloadedFiles.contains(userId)) {
				continue;
			}
			try {
				String fileName = "followers//user_"+userId+"_followers"+".json";
				FileWriter writer = new FileWriter(fileName);
				//this has only 15 hits per 15 mins
				//String url = "https://api.twitter.com/2/users/"+user.getId()+"/followers?max_results=1000&user.fields=id,verified,verified_type,created_at";
				
				//15 hits per 15 mins
				String url = "https://api.twitter.com/1.1/followers/list.json?user_id="+userId+"&skip_status=true&include_user_entities=false&count=200";
				//System.out.println(url);
				Response response = new ApiClient().callApi(url);	
				System.out.println(count+"  "+userId+"response code="+response.code());
				String body = response.body().string();
				
				if(response.code()==429) {
					try {
						System.out.println(userId+"----- sleeping");
					    Thread.sleep(16 * 60 * 1000); // sleep for 15 minutes
					} catch (InterruptedException e) {
					    // handle the exception
					}
					//retry once
					response = new ApiClient().callApi(url);
					body = response.body().string();
					if(body.contains("Too Many Requests")) {
						System.out.println("ERROR  : retry failed");
						System.exit(1);
					}
					//System.out.println(fileName+"---"+body);
					writer.write( body	+"\n");
					
					if (writer != null) {
						writer.flush();
						writer.close();
					}
				}else {
					writer.write( body	+"\n");					
					if (writer != null) {
						writer.flush();
						writer.close();
					}
				}
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			alreadyDownloadedFiles.add(userId);
		}
		
	}
	
	
	public static void processUsersSaveToDb() {
		List<String> files = getFileNamesInDirectory(fileLocationUsers);
		int count = 0;
		try {
			for (String fileName : files) {
				System.out.println(fileName+" count="+count);
				JsonNode jsonNode = objectMapper.readTree(new File(fileLocationUsers + "\\" + fileName));
				if (jsonNode != null) {
					JsonNode data = jsonNode.get("data");
					if (data != null) {
						for (JsonNode user : data) {
							JsonNode metrics = user.get("public_metrics");
							String id = user.get("id").asText();
							if (metrics != null) {
								long followers = metrics.get("followers_count").asLong();
								long following = metrics.get("following_count").asLong();
								long tweets = metrics.get("tweet_count").asLong();
								long listed = metrics.get("listed_count").asLong();
								//System.out.println(	id + "  ====:  " + followers + " " + following + " " + tweets + " " + listed);
								JdbcManager.updateTwitterUser(followers, following, tweets, listed, id);
								count++;
							}

						}

					}
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("finished");
	}

	public static void checkFirstFollowerData() {
		List<String> files = getFileNamesInDirectory(fileLocation);
		System.out.println(files.size());
		readFile(files);
	}

	public static void readFile(List<String> files) {

		int count = 0;
		try {
			for (String fileName : files) {
				// Read the JSON file and parse it into a JsonNode
				JsonNode jsonNode = objectMapper.readTree(new File(fileLocation + "\\" + fileName));

				// Access the JSON data
				// String nextCursorStr = jsonNode.get("next_cursor_str").asText();
				if (jsonNode.get("next_cursor") != null) {
					int nextCursor = jsonNode.get("next_cursor").asInt();

					JsonNode users = jsonNode.get("users");
					System.out.println(fileName + "   " + users.size());
					if (nextCursor > 0) {
						count++;
					}

				} else {
					System.out.println(fileName + " ###################  ");
				}

			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println(count);
	}

	public static List<String> getFileNamesInDirectory(String directoryPath) {
		File directory = new File(directoryPath);
		List<String> fileNames = new ArrayList<String>();
		if (directory.isDirectory()) {
			File[] files = directory.listFiles();

			for (File file : files) {
				if (file.isFile()) {
					fileNames.add(file.getName());
				}
			}
		} else {
			System.out.println("Invalid directory path.");
		}
		return fileNames;
	}
}
