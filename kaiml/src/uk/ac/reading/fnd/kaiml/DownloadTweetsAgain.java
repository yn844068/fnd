package uk.ac.reading.fnd.kaiml;

import java.io.FileWriter;
import java.util.List;

import okhttp3.Response;
import uk.ac.reading.fnd.kaiml.rest.ApiClient;

public class DownloadTweetsAgain {
	private static final int BATCH_SIZE = 96;

	public static void main(String[] args) throws Exception {
		System.out.println("DownloadTweetsAgain");
		String fileName = "D:\\data-science\\project\\experiments\\twitter-data-p2-all.csv";
		List<String> tweetIds = Utils.readColumnInCSVFileIntoList(fileName, 0);
		System.out.println(tweetIds.size());
//		for (int i = 0; i < Math.min(10, tweetIds.size()); i++) {
//			// String output = input.replaceAll("^\"|\"$", "");
//			//System.out.println(tweetIds.get(i));
//		}

		List<List<String>> batches = Utils.batch(tweetIds, BATCH_SIZE);
		System.out.println("number of batches=" + batches.size() + "\n");
		int batchCount = 0;
		
		for (List<String> batch : batches) {
			batchCount++;
			if(batchCount < 7370) {
				continue;
			}
			System.out.println("starting -from "+batchCount);

			String outFileName = "D:\\data-science\\project\\experiments\\tweets3\\tweet_batch_"+batchCount+".json";
			System.out.println("GET tweets?ids API "+outFileName+" "+batch.size());
			FileWriter writer = new FileWriter(outFileName);
			String concatenatedTweetIds = String.join(",", batch);
			String url = "https://api.twitter.com/1.1/statuses/lookup.json?id="+concatenatedTweetIds;
			//System.out.println(url);
			Response response = null;
			try {
				boolean success = false;
				while(!success) {
					response = new ApiClient().callApi(url);
					//System.out.println(" ------------ response "+response);
					String body = response.body().string();
					int statusCode = response.code();
		            System.out.println("Response status boom code: " + statusCode);
		            //System.out.println("boom leng "+body.length());
		            //System.out.println(body.substring(0, 141160));
					
					if(statusCode != 200) {
						System.out.println("-------boom-------  Response:"+body);
						try {
							System.out.println(batchCount+"----- sleeping");
						    Thread.sleep(15 * 60 * 1001); // sleep for 15 minutes
						} catch (InterruptedException e) {
						    // handle the exception
						}
					}else {
						success = true;
						writer.write( body	+"\n");
					}
				}
				
				


				if (writer != null) {
					writer.flush();
					writer.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}
