package uk.ac.reading.fnd.kaiml;

import java.util.Arrays;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

public class GetFollowerIds {
    public static void main(String[] args) {
        System.out.println("This example get the number of followers");

        /*
         * Latest keys
         */

        // set up authorization using your Twitter API credentials
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true)
          .setOAuthConsumerKey("huZVr1PwKWUWpqTOaYazBpZ28")
          .setOAuthConsumerSecret("1XEw4GwqJnKNsJQOatxJNKyk98nZv4fWFQoBzdiDYp0gtW3bpI")
          .setOAuthAccessToken("1614712013623595009-jyTDFabeaOKQYI1QHujDqTpvXgKwSU")
          .setOAuthAccessTokenSecret("pYboh83KmvDojLIbCh0MeHlwb01tJNHVh22DywgrZ8O0M");

        TwitterFactory tf = new TwitterFactory(cb.build());
        Twitter twitter = tf.getInstance();

        // Get the user_id of the user whose followers you want to retrieve
        long user_id = 193887043L;

        // Use Twitter4J to get a list of the user's follower IDs
        long[] follower_ids;
        try {
            follower_ids = twitter.getFollowersIDs(user_id, -1).getIDs();
            for (long id : follower_ids) {
                System.out.println(id);
            }
            System.out.println(Arrays.toString(follower_ids));
        } catch (TwitterException e) {
            e.printStackTrace();
            return;
        }

        // Use Twitter4J to get a list of the user objects for the follower IDs
        int num_followers = follower_ids.length;
        System.out.println(num_followers);
    }
}
