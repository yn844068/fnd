package uk.ac.reading.fnd.kaiml;

import java.util.Set;

public class NewsItem {
	private String id;
	private String newsUrl;
	private String title;
	private Boolean flagReal;
	private Set<String> tweets;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNewsUrl() {
		return newsUrl;
	}
	public void setNewsUrl(String newsUrl) {
		this.newsUrl = newsUrl;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Set<String> getTweets() {
		return tweets;
	}
	public void setTweets(Set<String> tweets) {
		this.tweets = tweets;
	}
	public Boolean getFlagReal() {
		return flagReal;
	}
	public void setFlagReal(Boolean flagReal) {
		this.flagReal = flagReal;
	}
		
	public NewsItem() {
	}
	public NewsItem(String id, String newsUrl, String title, Boolean flagReal, Set<String> tweets) {
		super();
		this.id = id;
		this.newsUrl = newsUrl;
		this.title = title;
		this.flagReal = flagReal;
		this.tweets = tweets;
	}
	
	
	@Override
	public String toString() {
		if(tweets == null) {
			return id+" "+newsUrl+" "+title;
		}else {
			return id+" "+newsUrl+" "+title+" "+tweets.size();
		}
	}
}
