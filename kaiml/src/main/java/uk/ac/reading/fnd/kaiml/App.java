package uk.ac.reading.fnd.kaiml;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.org.apache.bcel.internal.generic.DADD;

import okhttp3.Response;
import uk.ac.reading.fnd.kaiml.jdbc.JdbcManager;
import uk.ac.reading.fnd.kaiml.models.InvalidResource;
import uk.ac.reading.fnd.kaiml.models.Tweet;
import uk.ac.reading.fnd.kaiml.models.User;
import uk.ac.reading.fnd.kaiml.rest.ApiClient;


/**
 * Hello world!
 *
 */
public class App {
	private static final Logger logger = LoggerFactory.getLogger(App.class);
	private static final String OUT_FILE_PREFIX = "data//article_";
	private static final int BATCH_SIZE = 90;
	private static ObjectMapper objectMapper = new ObjectMapper();

	public static void main(String[] args) throws Exception {
		Set<String> downloaded = Utils.readFileLineByLine("darticles.txt");
		List<NewsItem> articles = JdbcManager.getArticles("select * from articles ");
		System.out.println("Number of articles "+articles.size());
		try {
			
			for(NewsItem article : articles) {
				
				if(downloaded.contains(article)) {
					continue;
				}
				
				System.out.println(""+article+"  ");
				logger.info("----------" + article+"\n");
				String type = "";
				if(article.getFlagReal()) {
					type = "real";
				}else {
					type = "fake";
				}
				List<String> tweetIds = JdbcManager.getTweetIdsByArticleId(article.getId());
				System.out.println(tweetIds.size()+" number of tweets ");
				if(tweetIds.size() == 0) {
					logger.info("----------skipping article, no tweets \n");
					continue;
				}
				
				
		
				List<List<String>> batches = Utils.batch(tweetIds, BATCH_SIZE);
				List<String> fileNames = new ArrayList<String>();
				logger.info("number of batches="+batches.size()+"\n");
				int batchCount = 0;
				for(List<String> batch : batches) {
					String fileName = OUT_FILE_PREFIX+article.getId()+"_"+type+"_"+batchCount+".json";
					fileNames.add(fileName);
					FileWriter writer = new FileWriter(fileName);
					String concatenatedTweetIds = String.join(",", batch);
					String url = "https://api.twitter.com/2/tweets?ids="+concatenatedTweetIds+"&expansions=author_id&user.fields=id,verified,verified_type,created_at&tweet.fields=referenced_tweets,source,conversation_id,possibly_sensitive";
					logger.info("GET tweets?ids API "+batch.size()+" \n");
					Response response = null;
					try {
						response = new ApiClient().callApi(url);
						System.out.println("response code="+response.code());
						String body = response.body().string();
						if(body.contains("Too Many Requests")) {
							try {
								System.out.println(article.getId()+"----- sleeping");
							    Thread.sleep(16 * 60 * 1000); // sleep for 15 minutes
							} catch (InterruptedException e) {
							    // handle the exception
							}
							//retry once
							response = new ApiClient().callApi(url);
							body = response.body().string();
							if(body.contains("Too Many Requests")) {
								System.out.println("ERROR  : retry failed");
								System.exit(1);
							}
	
						}
						
						writer.write( body	+"\n");
	
						batchCount++;
						if (writer != null) {
							writer.flush();
							writer.close();
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

				
				//System.out.println(fileNames);
				for(String fileName : fileNames) {
					JsonNode rootNode = objectMapper.readTree(new File(fileName));
					JsonNode datasNode = rootNode.get("data");
					if(datasNode != null) {
						logger.info("dataNodes: " + datasNode.size()+"\n");
						List<Tweet> tweets = new ArrayList<Tweet>();
				        for (JsonNode dataNode : datasNode) {
				        	Tweet tweet = new Tweet();
				        	tweet.setText(dataNode.get("text").asText());
				        	tweet.setId(dataNode.get("id").asText());
				        	tweet.setConversationId(dataNode.get("conversation_id").asText());
				        	tweet.setAuthorId(dataNode.get("author_id").asText());
				        	if(dataNode.get("edit_history_tweet_ids") != null) {
					        	String editIds = dataNode.get("edit_history_tweet_ids").asText();
					        	if(dataNode.get("edit_history_tweet_ids").size() > 1) {
					            	tweet.setEditHistoryTweetIds(String.join(",", editIds));        		
					        	}else if(dataNode.get("edit_history_tweet_ids").size() == 1) {
					            	tweet.setEditHistoryTweetIds(""+dataNode.get("edit_history_tweet_ids").get(0));
					        	}				        		
				        	}
				        	tweet.setPossiblySensitive(dataNode.get("possibly_sensitive").asBoolean());
				        	JdbcManager.insertTwitterTweets(tweet);
				        	tweets.add(tweet);
				        }
				        logger.info("Number of tweets populated to db "+tweets.size()+"\n");
					}
					
			        
			        JsonNode includeNode = rootNode.get("includes");
			        JsonNode usersNode = null;
			        if(includeNode != null) {
				        usersNode = includeNode.get("users");			        	
			        }
			        
			        if(usersNode != null) {
				        List<User> users = new ArrayList<User>();
				        for (JsonNode userNode : usersNode) {
				            User user = new User();
				        	user.setId(userNode.get("id").asText());
				        	user.setName(userNode.get("name").asText());
				        	user.setVerified(userNode.get("verified").asBoolean());
				        	user.setCreatedAt(userNode.get("created_at").asText());
				        	user.setVerifiedType(userNode.get("verified_type").asText());
				        	user.setUsername(userNode.get("username").asText());
				        	JdbcManager.insertTwitterUser(user);
				        	users.add(user);
				        }
				        logger.info("Number of users populated to db "+users.size()+"\n");
			        	
			        }
			        

			        JsonNode errorsNode = rootNode.get("errors");
			        if(errorsNode != null) {
				        List<InvalidResource> errors = new ArrayList<InvalidResource>();
				        for (JsonNode errorNode : errorsNode) {
				        	InvalidResource error = new InvalidResource();
				        	error.setResourceId(errorNode.get("resource_id").asText());
				        	error.setParameter(errorNode.get("resource_id").asText());
				        	error.setResourceType(errorNode.get("resource_id").asText());
				        	error.setSection(errorNode.get("resource_id").asText());
				        	error.setTitle(errorNode.get("resource_id").asText());
				        	error.setValue(errorNode.get("resource_id").asText());
				        	error.setDetail(errorNode.get("resource_id").asText());
				        	error.setType(errorNode.get("resource_id").asText());
				        	JdbcManager.insertTwitterErrors(error);
				        	errors.add(error);
				        }
				        logger.info("Number of errors populated to db "+errors.size()+"\n");			        	
			        }
				
				}
			}
			
		
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		/*
		articles = JdbcManager.getArticles("select * from articles where id like 'polit%' and flag_real=false");
		System.out.println(articles.size());
		
		articles = JdbcManager.getArticles("select * from articles where id like 'gos%' and flag_real=true");
		System.out.println(articles.size());
		
		articles = JdbcManager.getArticles("select * from articles where id like 'gos%' and flag_real=false");
		System.out.println(articles.size());
		*/

	}
	
	public static void main3(String[] args) throws Exception {
		populateDB();
	}
	
	public static void main2(String[] args) throws Exception {
		List<NewsItem> articles = JdbcManager.getArticles("select * from articles where id like 'polit%' and flag_real=true");
		System.out.println(articles.size());
		
		//List<NewsItem> articles = parseFile("resources\\\\politifact_fake.csv");
		//processArticles(articles);
		//callAPI();
		FileWriter writer = new FileWriter("");
		writer.write("----------" + "\n");
		logger.info("=========sddds");
		if (writer != null) {
			writer.flush();
			writer.close();
		}
		if(BATCH_SIZE == BATCH_SIZE) {
			return;
		}
		
		JsonNode rootNode = objectMapper.readTree(new File("resources\\\\tweet75response2.json"));
		JsonNode datasNode = rootNode.get("data");
		logger.info("dataNode: " + datasNode.size());
		List<Tweet> tweets = new ArrayList<Tweet>();
        for (JsonNode dataNode : datasNode) {
        	Tweet tweet = new Tweet();
        	tweet.setText(dataNode.get("text").asText());
        	tweet.setId(dataNode.get("id").asText());
        	tweet.setConversationId(dataNode.get("conversation_id").asText());
        	tweet.setAuthorId(dataNode.get("author_id").asText());
        	String editIds = dataNode.get("edit_history_tweet_ids").asText();
        	if(dataNode.get("edit_history_tweet_ids").size() > 1) {
            	tweet.setEditHistoryTweetIds(String.join(",", editIds));        		
        	}else if(dataNode.get("edit_history_tweet_ids").size() == 1) {
            	tweet.setEditHistoryTweetIds(""+dataNode.get("edit_history_tweet_ids").get(0));
        	}
        	tweet.setPossiblySensitive(dataNode.get("possibly_sensitive").asBoolean());
        	JdbcManager.insertTwitterTweets(tweet);
        	tweets.add(tweet);
        }
        System.out.println("tweets="+tweets.size());
        
        
        JsonNode includeNode = rootNode.get("includes");
        JsonNode usersNode = includeNode.get("users");
        
        List<User> users = new ArrayList<User>();
        for (JsonNode userNode : usersNode) {
            User user = new User();
        	user.setId(userNode.get("id").asText());
        	user.setName(userNode.get("name").asText());
        	user.setVerified(userNode.get("verified").asBoolean());
        	user.setCreatedAt(userNode.get("created_at").asText());
        	user.setVerifiedType(userNode.get("verified_type").asText());
        	user.setUsername(userNode.get("username").asText());
        	JdbcManager.insertTwitterUser(user);
        	users.add(user);
        }
        System.out.println("users="+users.size());
        
        JsonNode errorsNode = rootNode.get("errors");
        List<InvalidResource> errors = new ArrayList<InvalidResource>();
        for (JsonNode errorNode : errorsNode) {
        	InvalidResource error = new InvalidResource();
        	error.setResourceId(errorNode.get("resource_id").asText());
        	error.setParameter(errorNode.get("resource_id").asText());
        	error.setResourceType(errorNode.get("resource_id").asText());
        	error.setSection(errorNode.get("resource_id").asText());
        	error.setTitle(errorNode.get("resource_id").asText());
        	error.setValue(errorNode.get("resource_id").asText());
        	error.setDetail(errorNode.get("resource_id").asText());
        	error.setType(errorNode.get("resource_id").asText());
        	JdbcManager.insertTwitterErrors(error);
        	errors.add(error);
        }
        System.out.println("errors="+errors.size());
	}

	private static void callAPI() throws Exception {
		String url = "https://api.twitter.com/2/tweets?ids=960225064590327808,944202537976156160,960944324107276294,937429898670600192,953420038819733504,941044474905423872,937452013939494912,937427631661768704,944057586374103045,939272836887261186,946917851243393024,937481780088598528,937628495865196544,961049833208532995,937436145004302337,941080636852523013,960215720071188480,961021342375202817,961049817886707712,937406789686980608,940987185859383296,960208102820859904,937688299757588485,942421857634004998,958020350599577600,938079298157760512,937787346019340288,960935344068841472,959850069037109250,960241750102159361,938211896431448064,939053016010735616,938776353049939968,938289136703823872,937462176293437443,937936039510990848,937508442352373760,952709650444668928,938988970653786112,946935469216059393,937524138121478144,937384406511005696,947133585886269440,937757406129479682,952689244400734208,937692378604941317,937929101620404224,937845358650101761,961639675277529094,944582624282120193,938008154125885440,937738398080622592,937693047458025474,937808118695608320,937855193483501568,937493781271732229,937813631281934337,942182775024734208,960005452980523009,937379378006282240,937913933414981632,937491440082522117,961355708750090240,960639325242347520,939445960865968128,937520726369816576,942750575267106817,937438119468699648,938600814914031616,937764315544907782,940980953299783680,960161342651998208,959796911778693120,960967219365797888,945092931844804608&expansions=author_id&user.fields=id,verified,verified_type,created_at&tweet.fields=referenced_tweets,source,conversation_id,possibly_sensitive"  ;
		Response response = new ApiClient().callApi(url);
		System.out.println( response.body().string());
	}
	
	
	private static void processArticles(List<NewsItem> articles) {
		for(NewsItem article : articles) {
			System.out.println(article);
			List<String> tweets = new ArrayList<String>();
			System.out.println("total tweets "+articles.size());
			if(article.getTweets() != null) {
				tweets.addAll(article.getTweets());				
			}
			List<List<String>> batches = Utils.batch(tweets, BATCH_SIZE);
			for(List<String> batch : batches) {
				System.out.println("batch="+batch.size());
				String concatenatedTweetIds = String.join(",", batch);
				String url = "https://api.twitter.com/2/tweets?ids="+concatenatedTweetIds;
				System.out.println(url);
			}
		}
	}
	
	
	public static void populateDB() throws Exception{
		List<String> myList = new ArrayList<String>(Arrays.asList("resources//gossipcop_fake.csv", 
				"resources//gossipcop_real.csv", 
				"resources//politifact_real.csv", "resources//politifact_fake.csv"));
		for(String fName : myList) {
			System.out.println("------------- fileName "+fName);
			List<NewsItem> articles = parseFile(fName);
			System.out.println("size of articles "+articles.size());
			Map<String, NewsItem> map = new HashMap<String, NewsItem>();
			for(NewsItem article : articles) {
				//System.out.println(map.size()+"  "+article);
				
				JdbcManager.insertArticle(article);
				JdbcManager.insertArticleTweet(article.getId(), article.getTweets());
			}
				
		}
	}
	
	private static List<NewsItem> parseFile(String fileName) {
		boolean flagReal = false;
		if(fileName.contains("real")) {
			flagReal = true;
		}
		List<NewsItem> list = new ArrayList<NewsItem>();
		try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
			String line;
			
			while ((line = br.readLine()) != null) {
				
				if(line.startsWith("id")) {
					continue;
				}
				
				// process the line
				String[] columns = line.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)", -1);
				if(columns.length != 4) {
					System.out.println(line);
					System.out.println(columns.length+"##########   ERROR   #############");
					continue;
				}
				NewsItem item = new NewsItem();
				item.setId(columns[0]);
				item.setNewsUrl(columns[1]);
				item.setTitle(columns[2]);
				item.setFlagReal(flagReal);
				String[] tweets = columns[3].split("\\s+");
				Set<String> tweetList = new HashSet<String>();
				for(String tweet : tweets) {
					if(! tweet.trim().isEmpty()) {
						tweetList.add(tweet.trim());						
					}
				}
				item.setTweets(tweetList);
				list.add(item);
				//System.out.println(item.getId()+"  tweets="+tweetList.size());
				
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return list;
	}
}
