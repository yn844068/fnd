package uk.ac.reading.fnd.kaiml.models;

public class User {
	private String id;
	private String username;
	private String name;
	private Boolean verified;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Boolean getVerified() {
		return verified;
	}
	public void setVerified(Boolean verified) {
		this.verified = verified;
	}
	public String getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	public String getVerifiedType() {
		return verifiedType;
	}
	public void setVerifiedType(String verifiedType) {
		this.verifiedType = verifiedType;
	}
	private String createdAt;
	private String verifiedType;
	
	@Override
	public String toString() {
		return id+" "+username+" "+name;
	}
}
